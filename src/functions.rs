// © 2022 Christoph Grenz <https://grenz-bonn.de>
//
// SPDX-License-Identifier: MPL-2.0

#![allow(non_snake_case)]
use super::sql_types::*;
use diesel::sql_types::*;

sql_function! {
	/// Represents the MySQL `ST_Dimension` function.
	#[sql_name="ST_Dimension"]
	fn ST_Dimension<G: Into<Geometry>>(g: G) -> Integer;
}

sql_function! {
	/// Represents the MySQL `ST_Envelope` function.
	#[sql_name="ST_Envelope"]
	fn ST_Envelope<G: Into<Geometry>>(g: G) -> Polygon;
}

sql_function! {
	/// Represents the MySQL `ST_GeometryType` function.
	#[sql_name="ST_GeometryType"]
	fn ST_GeometryType<G: Into<Geometry>>(g: G) -> Text;
}

sql_function! {
	/// Represents the MySQL `ST_IsEmpty` function.
	#[sql_name="ST_IsEmpty"]
	fn ST_IsEmpty<G: Into<Geometry>>(g: G) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_IsSimple` function.
	#[sql_name="ST_IsSimple"]
	fn ST_IsSimple<G: Into<Geometry>>(g: G) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_SRID` function (with one argument).
	#[sql_name="ST_SRID"]
	fn ST_SRID<G: Into<Geometry>>(g: G) -> Integer;
}

sql_function! {
	/// Represents the MySQL `ST_SRID` function (with two arguments).
	#[sql_name="ST_SRID"]
	fn ST_SRID_2<G: Into<Geometry>>(g: G, srid: Integer) -> G;
}

sql_function! {
	/// Represents the MySQL `ST_Transform` function.
	#[sql_name="ST_Transform"]
	fn ST_Transform<G: Into<Geometry>>(g: G, target_srid: Integer) -> G;
}

sql_function! {
	/// Represents the MySQL `ST_Latitude` function (with one argument).
	#[sql_name="ST_Latitude"]
	fn ST_Latitude(p: Point) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_Longitude` function (with one argument).
	#[sql_name="ST_Longitude"]
	fn ST_Longitude(p: Point) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_Latitude` function (with two arguments).
	#[sql_name="ST_Latitude"]
	fn ST_Latitude_2(p: Point, new_latitude: Double) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_Longitude` function (with two arguments).
	#[sql_name="ST_Longitude"]
	fn ST_Longitude_2(p: Point, new_longitude: Double) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_X` function (with one argument).
	#[sql_name="ST_X"]
	fn ST_X(p: Point) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_Y` function (with one argument).
	#[sql_name="ST_Y"]
	fn ST_Y(p: Point) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_X` function (with two arguments).
	#[sql_name="ST_X"]
	fn ST_X_2(p: Point, new_x: Double) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_Y` function (with two arguments).
	#[sql_name="ST_Y"]
	fn ST_Y_2(p: Point, new_y: Double) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_EndPoint` function
	#[sql_name="ST_EndPoint"]
	fn ST_EndPoint(ls: LineString) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_IsClosed` function
	#[sql_name="ST_IsClosed"]
	fn ST_IsClosed<G: Into<Geometry>>(ls: G) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Length` function
	#[sql_name="ST_Length"]
	fn ST_Length<G: Into<Geometry>>(ls: G) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_NumPoints` function
	#[sql_name="ST_NumPoints"]
	fn ST_NumPoints(ls: LineString) -> Integer;
}

sql_function! {
	/// Represents the MySQL `ST_PointN` function
	#[sql_name="ST_PointN"]
	fn ST_PointN(ls: LineString, n: Integer) -> Nullable<Point>;
}

sql_function! {
	/// Represents the MySQL `ST_StartPoint` function
	#[sql_name="ST_StartPoint"]
	fn ST_StartPoint(ls: LineString) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_Area` function
	#[sql_name="ST_Area"]
	fn ST_Area<G: Into<Geometry>>(poly: G) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_Centroid` function
	#[sql_name="ST_Centroid"]
	fn ST_Centroid<G: Into<Geometry>>(poly: G) -> Nullable<Point>;
}

sql_function! {
	/// Represents the MySQL `ST_ExteriorRing` function
	#[sql_name="ST_ExteriorRing"]
	fn ST_ExteriorRing(poly: Polygon) -> LineString;
}

sql_function! {
	/// Represents the MySQL `ST_ExteriorRing` function
	#[sql_name="ST_InteriorRingN"]
	fn ST_InteriorRingN(poly: Polygon, n: Integer) -> Nullable<LineString>;
}

sql_function! {
	/// Represents the MySQL `ST_NumInteriorRings` function
	#[sql_name="ST_NumInteriorRings"]
	fn ST_NumInteriorRings(poly: Polygon) -> Integer;
}

sql_function! {
	/// Represents the MySQL `ST_GeometryN` function
	#[sql_name="ST_GeometryN"]
	fn ST_GeometryN(gc: GeometryCollection, n: Integer) -> Nullable<Geometry>;
}

sql_function! {
	/// Represents the MySQL `ST_NumGeometries` function
	#[sql_name="ST_NumGeometries"]
	fn ST_NumGeometries(gc: GeometryCollection) -> Integer;
}

sql_function! {
	/// Represents the MySQL `ST_Buffer_Strategy` function (with one argument)
	#[sql_name="ST_Buffer_Strategy"]
	fn ST_Buffer_Strategy(strategy: Text) -> Blob;
}

sql_function! {
	/// Represents the MySQL `ST_Buffer_Strategy` function (with two arguments)
	#[sql_name="ST_Buffer_Strategy"]
	fn ST_Buffer_Strategy_2(strategy: Text, points_per_circle: Integer) -> Blob;
}

sql_function! {
	/// Represents the MySQL `ST_Buffer` function (with two arguments)
	#[sql_name="ST_Buffer"]
	fn ST_Buffer<G: Into<Geometry>>(g: G, d: Double) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_Buffer` function (with three arguments)
	#[sql_name="ST_Buffer"]
	fn ST_Buffer_1<G: Into<Geometry>>(g: G, d: Double, strategy1: Blob) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_Buffer` function (with four arguments)
	#[sql_name="ST_Buffer"]
	fn ST_Buffer_2<G: Into<Geometry>>(g: G, d: Double, strategy1: Blob, strategy2: Blob) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_Buffer` function (with five arguments)
	#[sql_name="ST_Buffer"]
	fn ST_Buffer_3<G: Into<Geometry>>(g: G, d: Double, strategy1: Blob, strategy2: Blob, strategy3: Blob) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_ConvexHull` function
	#[sql_name="ST_ConvexHull"]
	fn ST_ConvexHull<G: Into<Geometry>>(g: G) -> Nullable<Geometry>;
}

sql_function! {
	/// Represents the MySQL `ST_Difference` function
	#[sql_name="ST_Difference"]
	fn ST_Difference<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_Intersection` function
	#[sql_name="ST_Intersection"]
	fn ST_Intersection<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_SymDifference` function
	#[sql_name="ST_SymDifference"]
	fn ST_SymDifference<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_Union` function
	#[sql_name="ST_Union"]
	fn ST_Union<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_LineInterpolatePoint` function
	#[sql_name="ST_LineInterpolatePoint"]
	fn ST_LineInterpolatePoint(ls: LineString, fractional_distance: Double) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_LineInterpolatePoints` function
	#[sql_name="ST_LineInterpolatePoints"]
	fn ST_LineInterpolatePoints(ls: LineString, fractional_distance: Double) -> MultiPoint;
}

sql_function! {
	/// Represents the MySQL `ST_PointAtDistance` function
	#[sql_name="ST_PointAtDistance"]
	fn ST_PointAtDistance(ls: LineString, distance: Double) -> Point;
}

sql_function! {
	/// Represents the MySQL `ST_Contains` function
	#[sql_name="ST_Contains"]
	fn ST_Contains<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Crosses` function
	#[sql_name="ST_Crosses"]
	fn ST_Crosses<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Disjoint` function
	#[sql_name="ST_Disjoint"]
	fn ST_Disjoint<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Distance` function
	#[sql_name="ST_Distance"]
	fn ST_Distance<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Double;
}

sql_function! {
	/// Represents the MySQL `ST_Equals` function
	#[sql_name="ST_Equals"]
	fn ST_Equals<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Intersects` function
	#[sql_name="ST_Intersects"]
	fn ST_Intersects<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Overlaps` function
	#[sql_name="ST_Overlaps"]
	fn ST_Overlaps<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Nullable<Bool>;
}

sql_function! {
	/// Represents the MySQL `ST_Touches` function
	#[sql_name="ST_Touches"]
	fn ST_Touches<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Nullable<Bool>;
}

sql_function! {
	/// Represents the MySQL `ST_Within` function
	#[sql_name="ST_Within"]
	fn ST_Within<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRContains` function
	#[sql_name="MBRContains"]
	fn MBRContains<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRCoveredBy` function
	#[sql_name="MBRCoveredBy"]
	fn MBRCoveredBy<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRCovers` function
	#[sql_name="MBRCovers"]
	fn MBRCovers<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRDisjoint` function
	#[sql_name="MBRDisjoint"]
	fn MBRDisjoint<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRDisjoint` function
	#[sql_name="MBREquals"]
	fn MBREquals<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBROverlaps` function
	#[sql_name="MBROverlaps"]
	fn MBROverlaps<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRTouches` function
	#[sql_name="MBRTouches"]
	fn MBRTouches<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `MBRTouches` function
	#[sql_name="MBRWithin"]
	fn MBRWithin<G1: Into<Geometry>, G2: Into<Geometry>>(g1: G1, g2: G2) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Collect` aggregate function
	#[aggregate]
	#[sql_name = "ST_Collect"]
	fn ST_Collect<G: Into<Geometry>>(expr: G) -> Geometry;
}

sql_function! {
	/// Represents the MySQL `ST_IsValid` function
	#[sql_name = "ST_IsValid"]
	fn ST_IsValid<G: Into<Geometry>>(expr: G) -> Bool;
}

sql_function! {
	/// Represents the MySQL `ST_Validate` function
	#[sql_name = "ST_Validate"]
	fn ST_Validate<G: Into<Geometry> + NotNull>(expr: G) -> Nullable<G>;
}
